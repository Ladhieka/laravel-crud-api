<?php

use App\Http\Controllers\GrandparentController;
use App\Http\Controllers\ChildController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/grandparents', 'GrandparentController@get');

Route::get('grandparents', GrandparentController::class.'@get');


Route::get('grandparent/{id}', GrandparentController::class. '@getById');

Route::post('grandparent', GrandparentController::class. '@post');

Route::put('grandparent/{id}', GrandparentController::class. '@put');

Route::delete('grandparent/{id}', GrandparentController::class. '@delete');


Route::get('childs', ChildController::class. '@get');

Route::get('child/{id}', ChildController::class. '@getById');

Route::post('child', ChildController::class. '@post');

Route::put('child/{id}', ChildController::class. '@put');

Route::delete('child/{id}', ChildController::class. '@delete');
