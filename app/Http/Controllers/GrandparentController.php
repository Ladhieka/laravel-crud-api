<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Grandparent;

class GrandparentController extends Controller
{
    public function get() {
        $data = Grandparent::all();

        return response()->json(
            [ 
               "message" => "Success",
               "data" => $data
            ]
        );
    }

    public function getById($id) {
        $data = Grandparent::where('id', $id)->get();

        return response()->json(
            [ 
               "message" => "Success",
               "data" => $data
            ]
        );
    }

    public function post(Request $request) {
        $Grandparent = new Grandparent;
        $Grandparent->first_name = $request->first_name;
        $Grandparent->last_name = $request->last_name;
        $Grandparent->gender = $request->gender;
        $Grandparent->age = $request->age;

        $Grandparent->save();

        return response()->json(
            [ 
               "message" => "Success",
               "data" => $Grandparent
            ]
        );
    }

    public function put($id, Request $request) {
        $Grandparent = Grandparent::where('id', $id)->first();
        if($Grandparent){
            $Grandparent->first_name = $request->first_name ? $request->first_name : $Grandparent->first_name;
            $Grandparent->last_name = $request->last_name ? $request->last_name : $Grandparent->last_name;
            $Grandparent->gender = $request->gender ? $request->gender : $Grandparent->gender;
            $Grandparent->age = $request->age ? $request->age : $Grandparent->age;
    
            $Grandparent->save();
            return response()->json(
                [ 
                   "message" => "PUT Method Success " ,
                    "data" => $Grandparent
                ]
            );
        }
        return response()->json(
            [ 
               "message" => "Grandparent with id " . $id . " not found"
            ], 400
        );
    }

    public function delete($id) {

        $Grandparent = Grandparent::where('id', $id)->first();
        if($Grandparent){
            $Grandparent->delete();
            return response()->json(
                [ 
                   "message" => "DELETE Grandparent with id " . $id . " Success"
                ]
            );
        }
        return response()->json(
            [ 
                "message" => "Grandparent with id " . $id . " not found"
            ],400
        );
    }
}
