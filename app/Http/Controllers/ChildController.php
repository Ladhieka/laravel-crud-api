<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Child;

class ChildController extends Controller
{
    public function get() {
        $data = Child::all();

        return response()->json(
            [ 
               "message" => "Success",
               "data" => $data
            ]
        );
    }

    public function getById($id) {
        $data = Child::where('id', $id)->get();

        return response()->json(
            [ 
               "message" => "Success",
               "data" => $data
            ]
        );
    }

    public function post(Request $request) {
        $Child = new Child;
        $Child->grandparent_id = $request->grandparent_id;
        $Child->first_name = $request->first_name;
        $Child->last_name = $request->last_name;
        $Child->gender = $request->gender;
        $Child->age = $request->age;

        $Child->save();

        return response()->json(
            [ 
               "message" => "Success",
               "data" => $Child
            ]
        );
    }

    public function put($id, Request $request) {
        $Child = Child::where('id', $id)->first();
        if($Child){
            $Child->grandparent_id = $request->grandparent_id ? $request->grandparent_id :$Child->grandparent_id;
            $Child->first_name = $request->first_name ? $request->first_name : $Child->first_name;
            $Child->last_name = $request->last_name ? $request->last_name : $Child->last_name;
            $Child->gender = $request->gender ? $request->gender : $Child->gender;
            $Child->age = $request->age ? $request->age : $Child->age;
    
            $Child->save();
            return response()->json(
                [ 
                   "message" => "PUT Method Success " ,
                    "data" => $Child
                ]
            );
        }
        return response()->json(
            [ 
               "message" => "Child with id " . $id . " not found"
            ], 400
        );
    }

    public function delete($id) {

        $Child = Child::where('id', $id)->first();
        if($Child){
            $Child->delete();
            return response()->json(
                [ 
                   "message" => "DELETE Child with id " . $id . " Success"
                ]
            );
        }
        return response()->json(
            [ 
                "message" => "Child with id " . $id . " not found"
            ],400
        );
    }
}
